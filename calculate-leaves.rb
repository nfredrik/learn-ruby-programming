#calculate remaining VL and SL

def remaining_leaves(vl_taken, sl_taken)
  month_now = Time.now.month
  vl_remaining = ((month_now.to_f*1.25) - vl_taken.to_i)
  sl_remaining = ((month_now.to_f*1.25) - sl_taken.to_i)
  return "Remaining VL: #{vl_remaining.to_f} Remaining SL: #{sl_remaining.to_f}"
end

puts "How many VLs have you taken? "
my_vl = gets.chomp
puts "How many SLs have you taken? "
my_sl = gets.chomp

puts "#{remaining_leaves(my_vl,my_sl)}"