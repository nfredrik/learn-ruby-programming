#generate multiplication table

# version 1

(1..10).each { |y|
  (1..10).each { |x|
    print "#{x * y}  "
  }
  puts ''
}

# version 2

def multiply(m,n)
 (1..n).each do |x|
  o = ''
  (1..m).each do |y|
   o += (x*y).to_s + ' '
  end
  puts o
 end
end

multiply(10,10)